from django.contrib import admin
from .models import Pergunta, Opcao, Comentario, Usuario


class OpcaoInline(admin.TabularInline):
    model = Opcao
    extra = 1

class PerguntaAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['texto_pergunta']}),
        ('Informações de data', {'fields': ['data_de_publicacao']}),
        ('Informações de usuário', {'fields': ['autor']}),
    ]
    inlines = [OpcaoInline]
    list_display = ('texto_pergunta', 'id', 'data_de_publicacao', 'publicada_recentemente')
    list_filter = ['data_de_publicacao']
    search_fields = ['texto_pergunta']

admin.site.register(Pergunta, PerguntaAdmin)
admin.site.register(Usuario)
admin.site.register(Comentario)