import datetime
from django.utils import timezone
from django.urls import reverse
from .models import Pergunta
from django.test import TestCase

def criar_pergunta(texto, dias):
    """

    função para criação de uma pergunta para texto e data de publlicação

    """

    data = timezone.now() + datetime.timedelta(days = dias)
    return Pergunta.objects.create(texto_pergunta = texto, data_de_publicacao = data)


class IndexViewTest(TestCase):
    def test_sem_perguntas_cadastradas(self):
        """

        Teste para checar se há perguntas cadastradas IndexView. Caso não haja, será exibida uma mensagem de erro


        """

        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Não há enquetes cadastradas até o momento!")
        self.assertQuerysetEqual(resposta.context['pergunta_list'], [])

    def test_com_pergunta_no_passado(self):
        """

        teste da indexview exigindo normalmente pergunta no passado


        """

        criar_pergunta(texto='Pergunta no passado', dias = -30)
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertQuerysetEqual(resposta.context['pergunta_list'], ['<Pergunta: Pergunta no passado>'])

    def test_com_pergunta_no_futuro(self):
        """

        perguntas com datas de publicação no futuro não devem ser exibidas


        """

        criar_pergunta(texto="Pergunta no futuro", dias=1)
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Não há enquetes cadastradas até o momento!")
        self.assertQuerysetEqual(resposta.context['pergunta_list'], [])

    def test_com_pergunta_no_futuro_e_no_passado(self):
        """

        perguntas com datas de publicação no passado são exibidas e perguntas com data de publicação no futuro são omitidas


        """

        criar_pergunta(texto="Pergunta no futuro", dias=1)
        criar_pergunta(texto="Pergunta no passado", dias=-1)
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Pergunta no passado")
        self.assertQuerysetEqual(resposta.context['pergunta_list'], ['<Pergunta: Pergunta no passado>'])


    def test_duas_perguntas_no_passado(self):
        """

        exibe normalmente mais de uma pergunta com datas de publicação no passado.


        """

        criar_pergunta(texto="Pergunta no passado 1", dias=-1)
        criar_pergunta(texto="Pergunta no passado 2", dias=-5)
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Pergunta no passado")
        self.assertQuerysetEqual(resposta.context['pergunta_list'], ['<Pergunta: Pergunta no passado 1>', '<Pergunta: Pergunta no passado 2>'])

