import datetime
from django.utils import timezone
from django.urls import reverse
from .models import Pergunta
from django.test import TestCase


def criar_pergunta(texto, dias):
    """

    função para criação de uma pergunta para texto e data de publlicação

    """

    data = timezone.now() + datetime.timedelta(days = dias)
    return Pergunta.objects.create(texto_pergunta = texto, data_de_publicacao = data)


class DetalhesViewTest(TestCase):

    def test_id_invalido(self):
        """

        deverá retornar 404 ao informar um id invalido normalmente uma pergunta com data no passado.


        """

        resposta = self.client.get(reverse('main:detalhes', args=[99,]))
        self.assertEqual(resposta.status_code, 404)

    def test_pergunta_no_futuro(self):
        """

        deverá retornar um erro 404 ao indicar uma pergunta com data no futuro.


        """

        pergunta_futura = criar_pergunta(texto="Pergunta no futuro", dias=5)
        resposta = self.client.get(reverse('main:detalhes', args=[pergunta_futura.id,]))
        self.assertEqual(resposta.status_code, 404)

    def test_pergunta_no_passado(self):
        """

        deverá exibir normalmente uma pergunta com data no passado.


        """

        pergunta_passada = criar_pergunta(texto="Pergunta no passado", dias=-1)
        resposta = self.client.get(reverse('main:detalhes', args=[pergunta_passada.id,]))
        self.assertEqual(resposta.status_code, 200)





