from django.db import models
from django.utils import timezone
import datetime

class Usuario(models.Model):
    login = models.CharField(max_length=50)
    senha = models.CharField(max_length=50)
    email = models.CharField(max_length=100)
    def __str__(self):
        return self.login

class Pergunta(models.Model):
    texto_pergunta = models.CharField(max_length=200)
    data_de_publicacao = models.DateTimeField('Data de publicação')
    autor = models.ForeignKey(Usuario, on_delete=models.CASCADE, default = None, null=True)
    def __str__(self):
        return self.texto_pergunta
    def publicada_recentemente(self):
        agora = timezone.now()
        return agora-datetime.timedelta(days=1) <=self.data_de_publicacao <= agora
    publicada_recentemente.admin_order_field = 'data_de_publicacao'
    publicada_recentemente.boolean = True
    publicada_recentemente.short_description = 'Últimas 24H?'

class Opcao(models.Model):
    texto_opcao = models.CharField(max_length=200)
    votos = models.IntegerField(default=0)
    pergunta = models.ForeignKey(Pergunta, on_delete=models.CASCADE)
    def __str__(self):
        return self.texto_opcao

class Comentario(models.Model):
    pergunta_cometario = models.ForeignKey(Pergunta, related_name="comentarios", on_delete=models.CASCADE)
    nome_do_autor = models.CharField(max_length=255)
    texto = models.TextField()
    data_de_insercao = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.nome_do_autor
