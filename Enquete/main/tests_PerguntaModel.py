import datetime
from django.utils import timezone
from .models import Pergunta
from django.test import TestCase

# Create your tests here.
class PerguntaTeste(TestCase):
    def test_publicada_recentemente_com_pergunta_no_futuro(self):
        """
        o método recentemente precisa retornar false quando se tratar de perguntas com data de publicação no fututo.

        """
        data = timezone.now() + datetime.timedelta(seconds=1)
        pergunta_futura = Pergunta(data_de_publicacao = data)
        self.assertIs(pergunta_futura.publicada_recentemente(), False)

    def test_publicada_recentemente_com_data_anterior_a_24h_no_passado(self):
        """

        O método publicada recentemente deve retornar false quando se tratar de uma publicação anterior a 24h no passsado.

        """

        data = timezone.now() - datetime.timedelta(days=1, seconds=1)
        pergunta_passado = Pergunta(data_de_publicacao = data)
        self.assertIs(pergunta_passado.publicada_recentemente(), False)

    def test_publicada_recentemente_com_data_nas_ultimas_24hs(self):
        """

        O método publicada recentemente deve retornar true quando se tratar dentro de uma data de publicação dentro das últimas 24 horas


        """

        data = timezone.now() - datetime.timedelta(hours =23, minutes = 59, seconds = 59)
        pergunta_ok = Pergunta(data_de_publicacao = data)
        self.assertIs(pergunta_ok.publicada_recentemente(), True)
